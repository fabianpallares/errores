package errores

import (
	"fmt"
	"testing"
)

func TestError(t *testing.T) {
	e1 := Nuevo("mensaje de error")
	e2 := NuevoConError("e2", e1)
	fmt.Println("Desenvolver e2:", Desenvolver(e2))
	fmt.Printf("e1: %#v\n", e1)
	fmt.Println()
	fmt.Printf("e2: %#v\n", e2)
	fmt.Println("-------------------------")
	fmt.Printf("rastro: %#v\n", ObtenerRastro(e2))
}
