// Package errores proporciona mecanismos de error con Envolturas
// para poder realizar encadenamientos y seguimientos de errores.
package errores

import (
	"runtime"
	"strings"
)

// Rastro almacena el rastro donde se ha generado el error.
// Los datos guardados son: Nombre del paquete, archivo y número de línea.
type Rastro struct {
	Paquete string
	Archivo string
	Funcion string
	Linea   int
}

// EnvolturaError es la estructura de error que almacena un mensaje, un error
// interno y un rastro de la creación del error.
type EnvolturaError struct {
	mensaje string
	err     error
	rastro  Rastro
}

func (e *EnvolturaError) rastrear(profundidad int) {
	var rastro Rastro
	_, paquete, _, ok := runtime.Caller(profundidad)
	if ok {
		rastro.Paquete = paquete
	}

	pc, archivo, linea, ok := runtime.Caller(profundidad + 1)
	if ok {
		pos := strings.LastIndex(archivo, "/")
		rastro.Archivo = archivo[pos+1:]
		// rastro.Archivo, rastro.Linea = runtime.FuncForPC(pc).FileLine()

		funcion := runtime.FuncForPC(pc).Name()
		pos = strings.LastIndex(funcion, "/")
		rastro.Funcion = funcion[strings.LastIndex(funcion, ".")+1:]

		rastro.Linea = linea
	}

	e.rastro = rastro
}

// Error retorna el mensaje de error.
func (e *EnvolturaError) Error() string {
	return e.mensaje
}

// Desenvolver retorna el error interno.
func (e *EnvolturaError) Desenvolver() error {
	return e.err
}

// ObtenerRastro retorna el rastro del error.
func (e *EnvolturaError) ObtenerRastro() *Rastro {
	return &e.rastro
}

// Nuevo retorna un nuevo error, sólo con el mensaje de error recibido.
func Nuevo(mensaje string) error {
	e := &EnvolturaError{mensaje: mensaje}
	e.rastrear(1)
	return e
}

// NuevoConError retorna un nuevo error con el mensaje de error
// y el error interno recibido.
func NuevoConError(mensaje string, err error) error {
	e := &EnvolturaError{mensaje: mensaje, err: err}
	e.rastrear(1)
	return e
}

// Desenvolver retorna el error interno del error recibido en caso
// que este último, sea un tipo de error que posea la función Desenvolver().
func Desenvolver(err error) error {
	e, ok := err.(interface {
		Desenvolver() error
	})
	if !ok {
		return nil
	}

	return e.Desenvolver()
}

// ObtenerRastro retorna el rastro del error en caso que el error recibido
// sea un tipo de error que posea la función ObtenerRastro().
func ObtenerRastro(err error) *Rastro {
	e, ok := err.(interface {
		ObtenerRastro() *Rastro
	})
	if !ok {
		return nil
	}

	return e.ObtenerRastro()
}
